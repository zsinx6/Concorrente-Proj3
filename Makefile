all:
	@g++ -I/usr/local/include/opencv -I/usr/local/include/opencv2 -L/usr/local/lib/ -g -o seq  sequencial.cpp -lopencv_core -lopencv_imgproc -lopencv_highgui -lopencv_ml -lopencv_video -lopencv_features2d -lopencv_calib3d -lopencv_objdetect -lopencv_contrib -lopencv_legacy -lopencv_stitching && mpic++ -I/usr/local/include/opencv -I/usr/local/include/opencv2 -L/usr/local/lib/ -g -o para  paralelo.cpp -lopencv_core -lopencv_imgproc -lopencv_highgui -lopencv_ml -lopencv_video -lopencv_features2d -lopencv_calib3d -lopencv_objdetect -lopencv_contrib -lopencv_legacy -lopencv_stitching -fopenmp

cuda:
	@nvcc -I/usr/local/include/opencv -I/usr/local/include/opencv2 -L/usr/local/lib/ -g -o cu  paralelo.cu -lopencv_core -lopencv_imgproc -lopencv_highgui -lopencv_ml -lopencv_video -lopencv_features2d -lopencv_calib3d -lopencv_objdetect -lopencv_contrib -lopencv_legacy -lopencv_stitching
run:
	@gcc run.c -o run && ./run
runcuda:
	@gcc cuda.c -o cuda && ./cuda
clean:
	@rm para seq cu logs/seq/*.txt logs/para/*.txt logs/cu/*.txt run
exseq1cor:
	@./seq images/img1cor.jpg 2 >> logs/seq/img1cor.txt
exseq2cor:
	@./seq images/img2cor.jpg 2 >> logs/seq/img2cor.txt
exseq3cor:
	@./seq images/img3cor.jpg 2 >> logs/seq/img3cor.txt
exseq4cor:
	@./seq images/img4cor.jpg 2 >> logs/seq/img4cor.txt
exseq1gray:
	@./seq images/img1gray.jpg 1 >> logs/seq/img1gray.txt
exseq2gray:
	@./seq images/img2gray.jpg 1 >> logs/seq/img2gray.txt
exseq3gray:
	@./seq images/img3gray.jpg 1 >> logs/seq/img3gray.txt
exseq4gray:
	@./seq images/img4gray.jpg 1 >> logs/seq/img4gray.txt
expara1cor:
	@mpirun --hostfile hosts --bind-to none ./para images/img1cor.jpg 2 >> logs/para/img1cor.txt
expara2cor:
	@mpirun --hostfile hosts --bind-to none ./para images/img2cor.jpg 2 >> logs/para/img2cor.txt
expara3cor:
	@mpirun --hostfile hosts --bind-to none ./para images/img3cor.jpg 2 >> logs/para/img3cor.txt
expara4cor:
	@mpirun --hostfile hosts --bind-to none ./para images/img4cor.jpg 2 >> logs/para/img4cor.txt
expara1gray:
	@mpirun --hostfile hosts --bind-to none ./para images/img1gray.jpg 1 >> logs/para/img1gray.txt
expara2gray:
	@mpirun --hostfile hosts --bind-to none ./para images/img2gray.jpg 1 >> logs/para/img2gray.txt
expara3gray:
	@mpirun --hostfile hosts --bind-to none ./para images/img3gray.jpg 1 >> logs/para/img3gray.txt
expara4gray:
	@mpirun --hostfile hosts --bind-to none ./para images/img4gray.jpg 1 >> logs/para/img4gray.txt
excuda1cor:
	@./cu images/img1cor.jpg 2 >> logs/cu/img1cor.txt
excuda2cor:
	@./cu images/img2cor.jpg 2 >> logs/cu/img2cor.txt
excuda3cor:
	@./cu images/img3cor.jpg 2 >> logs/cu/img3cor.txt
excuda4cor:
	@./cu images/img4cor.jpg 2 >> logs/cu/img4cor.txt
excuda1gray:
	@./cu images/img1gray.jpg 1 >> logs/cu/img1gray.txt
excuda2gray:
	@./cu images/img2gray.jpg 1 >> logs/cu/img2gray.txt
excuda3gray:
	@./cu images/img3gray.jpg 1 >> logs/cu/img3gray.txt
excuda4gray:
	@./cu images/img4gray.jpg 1 >> logs/cu/img4gray.txt
