#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/time.h>
#define NTHREADS 1024

struct timeval tv1, tv2;

using namespace cv;

__global__ void calcula_soma(uchar *matriz, int row, int col){
    int i, j, a, b, soma, c;
    float media;
    int inicioi, fimi, inicioj, fimj, idi, idj;
    int divalt, divlarg;
    int id = threadIdx.x;
    //faz a divisao de tarefas entre as threads
    idi = id/32;
    idj = id;
    divalt = row/32; //divide a altura
    divlarg = col/32; //divide a largura
    inicioi = (idi*divalt)%row;
    fimi = inicioi + divalt;
    if(id >= 31*32) //saber se eh a ultima linha
        fimi = row;
    inicioj = (idj*divlarg)%col;
    fimj = inicioj + divlarg;
    if(id%32 == 31) //saber se eh a ultima coluna
        fimj = col;

    //repete o algoritmo do sequencial da mesma forma, porem cada thread executa sua parte
    for(i=inicioi;i<fimi;i++){
        for(j=inicioj;j<fimj;j++){
            soma = 0;
            c = 0;
            if(i==0 && j==0){
                for(a=0;a<3;a++){
                    for(b=0;b<3;b++){
                        soma = soma + matriz[a*col+b];
                        c++;
                    }
                }
            }
            else if(i==1 && j==0){
                for(a=0;a<4;a++){
                    for(b=0;b<3;b++){
                        soma = soma + matriz[a*col+b];
                        c++;
                    }
                }
            }
            else if(i==0 && j==1){
                for(a=0;a<3;a++){
                    for(b=0;b<4;b++){
                        soma = soma + matriz[a*col+b];
                        c++;
                    }
                }
            }
            else if(i==1 && j==1){
                for(a=0;a<4;a++){
                    for(b=0;b<4;b++){
                        soma = soma + matriz[a*col+b];
                        c++;
                    }
                }
            }
            else if(i==row-1 && j==col-1){
                for(a=row-3;a<row;a++){
                    for(b=col-3;b<col;b++){
                        soma = soma + matriz[a*col+b];
                        c++;
                    }
                }
            }
            else if(i==row-2 && j==col-1){
                for(a=row-4;a<row;a++){
                    for(b=col-3;b<col;b++){
                        soma = soma + matriz[a*col+b];
                        c++;
                    }
                }
            }
            else if(i==row-1 && j==col-2){
                for(a=row-3;a<row;a++){
                    for(b=col-4;b<col;b++){
                        soma = soma + matriz[a*col+b];
                        c++;
                    }
                }
            }
            else if(i==row-2 && j==col-2){
                for(a=row-4;a<row;a++){
                    for(b=col-4;b<col;b++){
                        soma = soma + matriz[a*col+b];
                        c++;
                    }
                }
            }
            else if(i==row-1 && j==0){
                for(a=row-3;a<row;a++){
                    for(b=0;b<3;b++){
                        soma = soma + matriz[a*col+b];
                        c++;
                    }
                }
            }
            else if(i==row-2 && j==0){
                for(a=row-4;a<row;a++){
                    for(b=0;b<3;b++){
                        soma = soma + matriz[a*col+b];
                        c++;
                    }
                }
            }
            else if(i==row-1 && j==1){
                for(a=row-3;a<row;a++){
                    for(b=0;b<4;b++){
                        soma = soma + matriz[a*col+b];
                        c++;
                    }
                }
            }
            else if(i==row-2 && j==1){
                for(a=row-4;a<row;a++){
                    for(b=0;b<4;b++){
                        soma = soma + matriz[a*col+b];
                        c++;
                    }
                }
            }
            else if(i==0 && j==col-1){
                for(a=0;a<3;a++){
                    for(b=col-3;b<col;b++){
                        soma = soma + matriz[a*col+b];
                        c++;
                    }
                }
            }
            else if(i==0 && j==col-2){
                for(a=0;a<3;a++){
                    for(b=col-4;b<col;b++){
                        soma = soma + matriz[a*col+b];
                        c++;
                    }
                }
            }
            else if(i==1 && j==col-1){
                for(a=0;a<4;a++){
                    for(b=col-3;b<col;b++){
                        soma = soma + matriz[a*col+b];
                        c++;
                    }
                }
            }
            else if(i==1 && j==col-2){
                for(a=0;a<4;a++){
                    for(b=col-4;b<col;b++){
                        soma = soma + matriz[a*col+b];
                        c++;
                    }
                }
            }
            else if(i==0 && (j>=2 && j<=col-3)){
                for(a=0;a<3;a++){
                    for(b=j-2;b<j+3;b++){
                        soma = soma + matriz[a*col+b];
                        c++;
                    }
                }
            }
            else if((i>=2 && i<=row-3) && j==0){
                for(a=i-2;a<i+3;a++){
                    for(b=0;b<3;b++){
                        soma = soma + matriz[a*col+b];
                        c++;
                    }
                }
            }
            else if((i>=2 && i<=row-3) && j==col-1){
                for(a=i-2;a<i+3;a++){
                    for(b=j-2;b<j+1;b++){
                        soma = soma + matriz[a*col+b];
                        c++;
                    }
                }
            }
            else if(i==row-1 && (j>=2 && j<=col-3)){
                for(a=i-2;a<i+1;a++){
                    for(b=j-2;b<j+3;b++){
                        soma = soma + matriz[a*col+b];
                        c++;
                    }
                }
            }
            else if(i==1 && (j>=2 && j<=col-3)){
                for(a=0;a<4;a++){
                    for(b=j-2;b<j+3;b++){
                        soma = soma + matriz[a*col+b];
                        c++;
                    }
                }
            }
            else if((i>=2 && i<=row-3) && j==1){
                for(a=i-2;a<i+3;a++){
                    for(b=0;b<4;b++){
                        soma = soma + matriz[a*col+b];
                        c++;
                    }
                }
            }
            else if((i>=2 && i<=row-3) && j==col-2){
                for(a=i-2;a<i+3;a++){
                    for(b=j-2;b<j+2;b++){
                        soma = soma + matriz[a*col+b];
                        c++;
                    }
                }
            }
            else if(i==row-2 && (j>=2 && j<=col-3)){
                for(a=i-2;a<i+2;a++){
                    for(b=j-2;b<j+3;b++){
                        soma = soma + matriz[a*col+b];
                        c++;
                    }
                }
            }
            else{
                for(a=i-2;a<i+3;a++){
                    for(b=j-2;b<j+3;b++){
                        soma = soma + matriz[a*col+b];
                        c++;
                    }
                }
            }
            if(c != 0){
                media = (float) soma/c;
                matriz[i*col+j] = (uchar) media;
            }
        }
    }
}

int main( int argc, char** argv ){
    Mat image, dst;
    if(argc != 3){
        printf("Error\n");
        return -1;
    }

    int color = atoi(argv[2])-1;
    if(strcmp(argv[2], "1") == 0)
        image = imread(argv[1], CV_LOAD_IMAGE_GRAYSCALE);
    else if(strcmp(argv[2], "2") == 0)
        image = imread(argv[1], CV_LOAD_IMAGE_COLOR);
    int ch = image.channels();
    int r = image.rows;
    int c = image.cols;
    int i, j, k;
    uchar* mat1 = NULL;
    uchar* mat2 = NULL;
    uchar* mat3 = NULL;
    uchar* mat1cu = NULL;
    uchar* mat2cu = NULL;
    uchar* mat3cu = NULL;

    //faz alocacao das matrizes em cpu e na gpu
    if(!color){
        mat1 = (uchar*) malloc(r*c*sizeof(uchar));
        cudaMalloc(&mat1cu, r*c*sizeof(uchar));
    }
    else{
        mat1 = (uchar*) malloc(r*c*sizeof(uchar));
        mat2 = (uchar*) malloc(r*c*sizeof(uchar));
        mat3 = (uchar*) malloc(r*c*sizeof(uchar));
        cudaMalloc(&mat1cu, r*c*sizeof(uchar));
        cudaMalloc(&mat2cu, r*c*sizeof(uchar));
        cudaMalloc(&mat3cu, r*c*sizeof(uchar));
    }

    gettimeofday(&tv1, NULL);

    //pega as informacoes da imagem e envia para a gpu
    if(!color){
        k=0;
        for(i=0; i<r; i++){
            for (j=0; j<c; j++){
                mat1[i*c+j] = image.data[k++];
            }
        }
        cudaMemcpy(mat1cu,mat1,r*c*sizeof(uchar),cudaMemcpyHostToDevice);
    }
    else{
        k = 0;
        for(i=0; i<r; i++){
            for (j=0; j<c; j++){
                mat1[i*c+j] = image.at<Vec3b>(i,j)[0];
                mat2[i*c+j] = image.at<Vec3b>(i,j)[1];
                mat3[i*c+j] = image.at<Vec3b>(i,j)[2];
                k++;
            }
        }
        cudaMemcpy(mat1cu,mat1,r*c*sizeof(uchar),cudaMemcpyHostToDevice);
        cudaMemcpy(mat2cu,mat2,r*c*sizeof(uchar),cudaMemcpyHostToDevice);
        cudaMemcpy(mat3cu,mat3,r*c*sizeof(uchar),cudaMemcpyHostToDevice);
    }

    //chama as funcoes na gpu, enviando NTHREADS para executar
    calcula_soma<<<1, NTHREADS>>>(mat1cu, r, c);
    if(color){
        calcula_soma<<<1, NTHREADS>>>(mat2cu, r, c);
        calcula_soma<<<1, NTHREADS>>>(mat3cu, r, c);
    }
    //Apos o algoritmo smooth, copia-se de volta os dados para memoria princial
    if(!color){
        cudaMemcpy(mat1,mat1cu,r*c*sizeof(uchar),cudaMemcpyDeviceToHost);
        k=0;
        for(i=0; i<r; i++){
            for(j=0; j<c; j++){
                image.data[k++] = mat1[i*c+j];
            }
        }
    }
    else{
        cudaMemcpy(mat1,mat1cu,r*c*sizeof(uchar),cudaMemcpyDeviceToHost);
        cudaMemcpy(mat2,mat2cu,r*c*sizeof(uchar),cudaMemcpyDeviceToHost);
        cudaMemcpy(mat3,mat3cu,r*c*sizeof(uchar),cudaMemcpyDeviceToHost);
        k=0;
        for(i=0; i<r; i++){
            for(j=0; j<c; j++){
                image.at<Vec3b>(i,j)[0] = mat1[i*c+j];
                image.at<Vec3b>(i,j)[1] = mat2[i*c+j];
                image.at<Vec3b>(i,j)[2] = mat3[i*c+j];
                k++;
            }
        }
    }

    //finalizada a escrita da image, escrever no disco:
    gettimeofday(&tv2, NULL);
    printf ("Total time = %f seconds\n",
            (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
            (double) (tv2.tv_sec - tv1.tv_sec));
    imwrite(argv[1], image);
    //desalocacao de memoria
    if(mat1 != NULL){
        free(mat1);
        cudaFree(mat1cu);
    }
    if(mat2 != NULL){
        free(mat2);
        cudaFree(mat2cu);
    }
    if(mat3 != NULL){
        free(mat3);
        cudaFree(mat3cu);
    }
    return 0;
}
