Projeto 3 de Programação Concorrente

Modo de usar:
É necessário ter um arquivo 'hosts' na raiz do projeto com o nome de pelo menos 3 nós antes de executar o programa.

Compilação:
make

Compilação do CUDA:
makecuda

Executar todos os testes para todas as imagens:
make run

Executar todos os testes para o programa CUDA:
make runcuda

Limpar os executáveis e os logs:
make clean

Executar para uma determinda imagem:
Imagem colorida: make exseqXcor OU make exparaXcor OU make excudaXcor
Imagem em escala de cinza: make exseqXgray OU make exparaXgray OU make excudaXgray

Onde o X representa o número da imagem.

Os logs ficam salvos em ./logs
As imagens encontram-se em ./images
