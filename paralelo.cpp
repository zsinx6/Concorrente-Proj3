#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <stdio.h>
#include <string.h>
#include <stdio.h>
#include <sys/time.h>
#include <omp.h>
#include "mpi.h"
#define NTHREADS 4

struct timeval tv1, tv2;

using namespace cv;

//faz a alocacao da matriz de forma linear
uchar **alloc_2d(int rows, int cols) {
    uchar *data = (uchar *)malloc(rows*cols*sizeof(uchar));
    uchar **array= (uchar **)malloc(rows*sizeof(uchar*));
    for (int i=0; i<rows; i++)
        array[i] = &(data[cols*i]);

    return array;
}

uchar** calcula_soma(uchar **matriz, int row, int col){
    omp_set_num_threads(NTHREADS);
    int i, j, a, b, soma, c;
    float media;
    int inicioi, fimi, inicioj, fimj, id;
    int divalt, divlarg;
    /*
        dividir essa parte em 4, uma para cada thread
        redefinir valores iniciais de i e j, para que cada for seja executado em um pedaço da imagem 
    */
    #pragma omp parallel private(soma,media,a,b,i,j,c,inicioi,fimi,inicioj,fimj,id,divalt,divlarg)
    {
        divalt = row/2; //divide a altura
        divlarg = col/2; //divide a largura
        id = omp_get_thread_num(); //pega o numero da thread, a primeira é 0, a segunda é 1...

        //faz a divisao de tarefas entre as threads
        if(id==0){
            inicioi = 0;
            inicioj = 0;
            fimi = divalt;
            fimj = divlarg;
        }
        else if(id==1){
            inicioi = 0;
            inicioj = divlarg;
            fimi = divalt;
            fimj = col;
        }
        else if(id==2){
            inicioi = divalt;
            inicioj = 0;
            fimi = row;
            fimj = divlarg;
        }
        else if(id==3){
            inicioi = divalt;
            inicioj = divlarg;
            fimi = row;
            fimj = col;
        }

        //repete o algoritmo do sequencial da mesma forma, porem cada thread executa sua parte
        for(i=inicioi;i<fimi;i++){
            for(j=inicioj;j<fimj;j++){
                soma = 0;
                c = 0;
                if(i==0 && j==0){
                    for(a=0;a<3;a++){
                        for(b=0;b<3;b++){
                            soma = soma + matriz[a][b];
                            c++;
                        }
                    }
                }
                else if(i==1 && j==0){
                    for(a=0;a<4;a++){
                        for(b=0;b<3;b++){
                            soma = soma + matriz[a][b];
                            c++;
                        }
                    }
                }
                else if(i==0 && j==1){
                    for(a=0;a<3;a++){
                        for(b=0;b<4;b++){
                            soma = soma + matriz[a][b];
                            c++;
                        }
                    }
                }
                else if(i==1 && j==1){
                    for(a=0;a<4;a++){
                        for(b=0;b<4;b++){
                            soma = soma + matriz[a][b];
                            c++;
                        }
                    }
                }
                else if(i==row-1 && j==col-1){
                    for(a=row-3;a<row;a++){
                        for(b=col-3;b<col;b++){
                            soma = soma + matriz[a][b];
                            c++;
                        }
                    }
                }
                else if(i==row-2 && j==col-1){
                    for(a=row-4;a<row;a++){
                        for(b=col-3;b<col;b++){
                            soma = soma + matriz[a][b];
                            c++;
                        }
                    }
                }
                else if(i==row-1 && j==col-2){
                    for(a=row-3;a<row;a++){
                        for(b=col-4;b<col;b++){
                            soma = soma + matriz[a][b];
                            c++;
                        }
                    }
                }
                else if(i==row-2 && j==col-2){
                    for(a=row-4;a<row;a++){
                        for(b=col-4;b<col;b++){
                            soma = soma + matriz[a][b];
                            c++;
                        }
                    }
                }
                else if(i==row-1 && j==0){
                    for(a=row-3;a<row;a++){
                        for(b=0;b<3;b++){
                            soma = soma + matriz[a][b];
                            c++;
                        }
                    }
                }
                else if(i==row-2 && j==0){
                    for(a=row-4;a<row;a++){
                        for(b=0;b<3;b++){
                            soma = soma + matriz[a][b];
                            c++;
                        }
                    }
                }
                else if(i==row-1 && j==1){
                    for(a=row-3;a<row;a++){
                        for(b=0;b<4;b++){
                            soma = soma + matriz[a][b];
                            c++;
                        }
                    }
                }
                else if(i==row-2 && j==1){
                    for(a=row-4;a<row;a++){
                        for(b=0;b<4;b++){
                            soma = soma + matriz[a][b];
                            c++;
                        }
                    }
                }
                else if(i==0 && j==col-1){
                    for(a=0;a<3;a++){
                        for(b=col-3;b<col;b++){
                            soma = soma + matriz[a][b];
                            c++;
                        }
                    }
                }
                else if(i==0 && j==col-2){
                    for(a=0;a<3;a++){
                        for(b=col-4;b<col;b++){
                            soma = soma + matriz[a][b];
                            c++;
                        }
                    }
                }
                else if(i==1 && j==col-1){
                    for(a=0;a<4;a++){
                        for(b=col-3;b<col;b++){
                            soma = soma + matriz[a][b];
                            c++;
                        }
                    }
                }
                else if(i==1 && j==col-2){
                    for(a=0;a<4;a++){
                        for(b=col-4;b<col;b++){
                            soma = soma + matriz[a][b];
                            c++;
                        }
                    }
                }
                else if(i==0 && (j>=2 && j<=col-3)){
                    for(a=0;a<3;a++){
                        for(b=j-2;b<j+3;b++){
                            soma = soma + matriz[a][b];
                            c++;
                        }
                    }
                }
                else if((i>=2 && i<=row-3) && j==0){
                    for(a=i-2;a<i+3;a++){
                        for(b=0;b<3;b++){
                            soma = soma + matriz[a][b];
                            c++;
                        }
                    }
                }
                else if((i>=2 && i<=row-3) && j==col-1){
                    for(a=i-2;a<i+3;a++){
                        for(b=j-2;b<j+1;b++){
                            soma = soma + matriz[a][b];
                            c++;
                        }
                    }
                }
                else if(i==row-1 && (j>=2 && j<=col-3)){
                    for(a=i-2;a<i+1;a++){
                        for(b=j-2;b<j+3;b++){
                            soma = soma + matriz[a][b];
                            c++;
                        }
                    }
                }
                else if(i==1 && (j>=2 && j<=col-3)){
                    for(a=0;a<4;a++){
                        for(b=j-2;b<j+3;b++){
                            soma = soma + matriz[a][b];
                            c++;
                        }
                    }
                }
                else if((i>=2 && i<=row-3) && j==1){
                    for(a=i-2;a<i+3;a++){
                        for(b=0;b<4;b++){
                            soma = soma + matriz[a][b];
                            c++;
                        }
                    }
                }
                else if((i>=2 && i<=row-3) && j==col-2){
                    for(a=i-2;a<i+3;a++){
                        for(b=j-2;b<j+2;b++){
                            soma = soma + matriz[a][b];
                            c++;
                        }
                    }
                }
                else if(i==row-2 && (j>=2 && j<=col-3)){
                    for(a=i-2;a<i+2;a++){
                        for(b=j-2;b<j+3;b++){
                            soma = soma + matriz[a][b];
                            c++;
                        }
                    }
                }
                else{
                    for(a=i-2;a<i+3;a++){
                        for(b=j-2;b<j+3;b++){
                            soma = soma + matriz[a][b];
                            c++;
                        }
                    }
                }
                if(c != 0){
                    media = (float) soma/c;
                    matriz[i][j] = (uchar) media;
                }
            }
        }
    }
    return matriz;
}

int main( int argc, char** argv ){
    Mat image, dst;

    //inicia a comunicacao com os nos e limpa o argc e o argv dos argumentos do mpi
    MPI_Init(&argc, &argv);
    int numtasks, taskid;

    //inicia a quantidade de nos
    //pega o id do no atual em taskid
    MPI_Comm_size(MPI_COMM_WORLD, &numtasks);
    MPI_Comm_rank(MPI_COMM_WORLD, &taskid);
    if(argc != 3){
        printf("Error\n");
        return -1;
    }
    int color = atoi(argv[2])-1;
    if(strcmp(argv[2], "1") == 0)
        image = imread(argv[1], CV_LOAD_IMAGE_GRAYSCALE);
    else if(strcmp(argv[2], "2") == 0)
        image = imread(argv[1], CV_LOAD_IMAGE_COLOR);
    int ch = image.channels();
    int r = image.rows;
    int c = image.cols;
    int i, j, k;
    int chunksize;
    uchar** mat1 = NULL;
    uchar** mat2 = NULL;
    uchar** mat3 = NULL;
    MPI_Status status;

    //faz alocacao das matrizes
    if(!color){
        mat1 = alloc_2d(r,c);
    }
    else{
        mat1 = alloc_2d(r,c);
        mat2 = alloc_2d(r,c);
        mat3 = alloc_2d(r,c);
    }

    //taskid 0 eh o id do master
    if(taskid == 0){
        gettimeofday(&tv1, NULL);

        //pega as informacoes da imagem
        if(!color){
            k=0;
            for(i=0; i<r; i++){
                for (j=0; j<c; j++){
                    mat1[i][j] = image.data[k++];
                }
            }
        }
        else{
            for(i=0; i<r; i++){
                for (j=0; j<c; j++){
                    mat1[i][j] = image.at<Vec3b>(i,j)[0];
                    mat2[i][j] = image.at<Vec3b>(i,j)[1];
                    mat3[i][j] = image.at<Vec3b>(i,j)[2];
                }
            }
        }

        //chama a funcao passando as matrizes e recebe de volta as mesmas alteradas
        //envia os vetores para os outros nós, cada nó recebe uma das mat, ou seja, uma cor e aplica o calcula_soma
        chunksize = r*c; //tamanho da matriz
        if(color){
            MPI_Send(&mat2[0][0], chunksize, MPI_UNSIGNED_CHAR, 1, 0, MPI_COMM_WORLD);
            MPI_Send(&mat3[0][0], chunksize, MPI_UNSIGNED_CHAR, 2, 0, MPI_COMM_WORLD);
        }

        //a master tambem calcula uma parte (r) da operacao
        mat1 = calcula_soma(mat1, r, c);

        //recebe os vetores de volta dos outros nos, reescrevendo os valores de mat2 e mat3
        if(color){
            chunksize = r*c;
            MPI_Recv(&mat2[0][0], chunksize, MPI_UNSIGNED_CHAR, 1, 0, MPI_COMM_WORLD, &status);
            MPI_Recv(&mat3[0][0], chunksize, MPI_UNSIGNED_CHAR, 2, 0, MPI_COMM_WORLD, &status);
        }

        //entao deve-se alterar a imagem atual para a nova
        if(!color){
            k=0;
            for(i=0; i<r; i++){
                for(j=0; j<c; j++){
                    image.data[k++] = mat1[i][j];
                }
            }
        }
        else{
            for(i=0; i<r; i++){
                for(j=0; j<c; j++){
                    image.at<Vec3b>(i,j)[0] = mat1[i][j];
                    image.at<Vec3b>(i,j)[1] = mat2[i][j];
                    image.at<Vec3b>(i,j)[2] = mat3[i][j];
                }
            }
        }

        //finalizada a escrita da image, escrever no disco:
        gettimeofday(&tv2, NULL);
        printf ("Total time = %f seconds\n",
                (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
                (double) (tv2.tv_sec - tv1.tv_sec));
        imwrite(argv[1], image);
        //desalocacao de memoria
        if(mat1 != NULL){
            free(mat1[0]);
            free(mat1);
        }
        if(mat2 != NULL){
            free(mat2[0]);
            free(mat2);
        }
        if(mat3 != NULL){
            free(mat3[0]);
            free(mat3);
        }
    }
    //no 1, so entra aqui se for o no 1 e se a imagem for colorida
    else if(taskid == 1 && color){
        //recebe a matriz
        chunksize = r*c;
        MPI_Recv(&mat2[0][0], chunksize, MPI_UNSIGNED_CHAR, 0, 0, MPI_COMM_WORLD, &status);
        //aplica a funcao
        mat2 = calcula_soma(mat2, r, c);
        //envia de volta a imagem
        chunksize = r*c;
        MPI_Send(&mat2[0][0], chunksize, MPI_UNSIGNED_CHAR, 0, 0, MPI_COMM_WORLD);

    }
    //no 2, so entra aqui se for o no 2 e se a imagem for colorida
    else if(taskid == 2 && color){
        //recebe a matriz
        chunksize = r*c;
        MPI_Recv(&mat3[0][0], chunksize, MPI_UNSIGNED_CHAR, 0, 0, MPI_COMM_WORLD, &status);
        //aplica a funcao
        mat3 = calcula_soma(mat3, r, c);
        //envia de volta a imagem
        chunksize = r*c;
        MPI_Send(&mat3[0][0], chunksize, MPI_UNSIGNED_CHAR, 0, 0, MPI_COMM_WORLD);

    }
    //finaliza a comunicacao com os nos
    MPI_Finalize();
    return 0;
}
